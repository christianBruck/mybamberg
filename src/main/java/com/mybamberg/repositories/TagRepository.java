package com.mybamberg.repositories;

import com.mybamberg.entities.Location;
import com.mybamberg.entities.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by christian on 08.05.16.
 */
public interface TagRepository extends JpaRepository<Tag, Long> {
    Tag findOne(Long tagID);
   Tag findByTagName(String tagName);
}
