package com.mybamberg.repositories;

import com.mybamberg.entities.Event;
import com.mybamberg.entities.Tag;
import com.mybamberg.entities.Weather;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by christian on 08.05.16.
 */
public interface WeatherRepository extends JpaRepository<Weather, Long> {

    Weather findByDate(Date date);

    Weather findOne(Long weatherID);

    @Override
    void deleteAll();

    List<Weather> findByDateBetween(Date t1, Date t2);

    @Query(value = "SELECT * FROM weather w WHERE w.date <= ?1 ORDER BY w.date DESC LIMIT 1", nativeQuery = true)
    Weather findOneByDateBefore(Date date);

}
