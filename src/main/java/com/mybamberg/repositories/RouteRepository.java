package com.mybamberg.repositories;

import com.mybamberg.entities.Event;
import com.mybamberg.entities.Route;
import com.mybamberg.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by christian on 13.06.16.
 */
public interface RouteRepository extends JpaRepository<Route, Long > {


    Route findOne(Long eventID);

    Route findByUser(User user);





}
