package com.mybamberg.repositories;

import com.mybamberg.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by christian on 19.07.16.
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {


    Category findByCategoryname(String categoryname);
}
