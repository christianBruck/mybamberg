package com.mybamberg.repositories;

import com.mybamberg.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by christian on 19.07.16.
 */
public interface UserRepository extends JpaRepository <User, Long> {

   User findByAndroidId(String androidId);
}
