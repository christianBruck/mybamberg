package com.mybamberg.repositories;

import com.mybamberg.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mybamberg.entities.Event;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * Created by christian on 01.05.16.
 */
public interface EventRepository extends JpaRepository<Event, Long> {



    List<Event> findByCategoryAndStartdateAfter(Category category,Date startDate);


    @Query("select e from Event e where e.category=?1 and e.startdate >= ?2 and e.startdate <= ?3 and e.enddate >= ?2 and e.enddate <= ?3")
    List<Event> findMyEvents(Category category,Date startDateUser,Date endDateUser);


}
