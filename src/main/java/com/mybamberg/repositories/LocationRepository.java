package com.mybamberg.repositories;

import com.mybamberg.entities.Event;
import com.mybamberg.entities.Location;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by christian on 08.05.16.
 */
public interface LocationRepository extends JpaRepository<Location,Long> {


    Location findOne(Long locationID);

}
