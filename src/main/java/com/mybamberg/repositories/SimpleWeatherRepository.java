package com.mybamberg.repositories;

import com.mybamberg.entities.SimpleWeather;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by christian on 19.07.16.
 */
public interface SimpleWeatherRepository extends JpaRepository<SimpleWeather,Long> {

    SimpleWeather findByName(String name);
}
