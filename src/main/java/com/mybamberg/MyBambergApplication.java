package com.mybamberg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MyBambergApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyBambergApplication.class, args);
	}
}
