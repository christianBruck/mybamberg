package com.mybamberg.entities;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName(value = "weather")

/**
 * Created by christian on 08.05.16.
 */
@Entity
public class Weather {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    //@Column(unique = true)
    //private String weathername;

    @Column
    private Integer weatherApiId;
    @Column
    private String description;

    @OneToOne
    private SimpleWeather simpleWeather;

    @Column(unique = true)
    private Date date;


    public Weather() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*public String getWeathername() {
        return weathername;
    }

    public void setWeathername(String weathername) {
        this.weathername = weathername;
    }*/

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWeatherApiId() {
        return weatherApiId;
    }

    public void setWeatherApiId(Integer weatherApiId) {
        this.weatherApiId = weatherApiId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public SimpleWeather getSimpleWeather() {
        return simpleWeather;
    }

    public void setSimpleWeather(SimpleWeather simpleWeather) {
        this.simpleWeather = simpleWeather;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Value{");
        sb.append("weatherApiId=").append(weatherApiId);
        sb.append(", description='").append(description).append('\'');
        sb.append(", date=").append(date);
        sb.append('}');
        return sb.toString();
    }

}
