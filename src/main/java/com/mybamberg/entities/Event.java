package com.mybamberg.entities;


import javax.persistence.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by christian on 01.05.16.
 */
@Entity
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(unique = true)
    private String eventname;

    @ManyToOne
    private Location location;

    @Column(name = "description")
    private String description;

    @OneToOne
    private Category category;

    @ManyToMany
    private List<Tag> taglist;

    @Column(name = "startdate")
    private Date startdate;

    @Column(name = "enddate")
    private Date enddate;

    private boolean valid;

    private String pictureURL;


    @OneToOne
    private SimpleWeather simpleWeather;






    public Event() {
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }


    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Tag> getTaglist() {
        return taglist;
    }

    public void setTaglist(List<Tag> taglist) {
        this.taglist = taglist;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }


    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public SimpleWeather getSimpleWeather() {
        return simpleWeather;
    }

    public void setSimpleWeather(SimpleWeather simpleWeather) {
        this.simpleWeather = simpleWeather;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", eventname='" + eventname + '\'' +
                ", location=" + location +
                ", description='" + description + '\'' +
                ", category=" + category +
                ", taglist=" + taglist +
                ", startdate=" + startdate +
                ", enddate=" + enddate +
                ", valid=" + valid +
                ", pictureURL='" + pictureURL + '\'' +
                ", simpleWeather=" + simpleWeather +
                '}';
    }
}
