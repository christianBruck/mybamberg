package com.mybamberg.webcontroller;

import com.mybamberg.entities.Event;
import com.mybamberg.repositories.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * Created by christian on 01.05.16.
 */
@Controller
public class EventControllerWeb {

    @Autowired
    private EventRepository eventsRepository;

    @RequestMapping("/")
    public String home(){
        return "index";
    }


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String homePost(){
        return "index";
    }

/*    @RequestMapping(value = "/createevent",method = RequestMethod.POST)
    public String createEvent(@RequestParam String eventname,@RequestParam String location){
        Event myEvent = new Event();

        return "index";
    }*/

}

