package com.mybamberg.restcontroller;

import com.mybamberg.dto.*;
import com.mybamberg.entities.Event;
import com.mybamberg.entities.User;
import com.mybamberg.repositories.UserRepository;
import com.mybamberg.services.EventService;
import com.mybamberg.services.RoutenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by christian on 08.06.16.
 */
@RestController
public class RoutenController {

    private Logger logger = LoggerFactory.getLogger(RoutenController.class);

    @Autowired
    private RoutenService routenService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EventService eventService;

    /**
     * This method expects an androidID as String and returns the events stored in a route-object
     * @param androidId
     * @return
     */
    @Transactional
    @RequestMapping(value ="/v1/route", method = RequestMethod.GET)
    public @ResponseBody  RouteDTO generateRoute(@RequestParam  String androidId) {
        RouteDTO routeDTO = eventService.getEventsForUser(androidId);
        logger.info("generated routeDTO" +routeDTO);
      return routeDTO;


    }

    /**
     * Client can POST his choosen Events and save them in the database
     * @param routeDTO
     * @return
     */
    @Transactional
    @RequestMapping(value = "/v1/finalroute", method = RequestMethod.POST)
    public @ResponseBody RouteDTO saveFinalRoute(@RequestBody  RouteDTO routeDTO) {
        logger.info("/v1/finalroute " + routeDTO);

        RouteDTO toReturn =  routenService.saveFinalRoute(routeDTO);

        return toReturn;
    }

    /**
     * For the given androidID the System will proove the feasibility of the eventList and update for each event the valid value
     * @param androidId
     * @return
     */

    @Transactional
    @RequestMapping(value ="/v1/updateroute", method = RequestMethod.GET)
    public @ResponseBody  RouteDTO updateRoute(@RequestParam  String androidId) {
        logger.info("enter /v1/updateroute = " +  androidId);
        RouteDTO finalRouteDTO=  routenService.checkRouteValidityByAndroidID(androidId);
        return finalRouteDTO;

    }



}
