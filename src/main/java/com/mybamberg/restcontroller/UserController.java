package com.mybamberg.restcontroller;

import com.mybamberg.dto.UserDTO;
import com.mybamberg.entities.User;
import com.mybamberg.repositories.UserRepository;
import com.mybamberg.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * Created by christian on 25.07.16.
 */
@RestController
public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    /**
     * The userDtO object stores the user settings and his androidId. This object will be
     * stored in the database and then send back to the client.
     * @param userDTO
     * @return
     */
    @Transactional
    @RequestMapping(value = "/v1/user", method = RequestMethod.POST)
    public @ResponseBody UserDTO checkAndSaveUser(@RequestBody  UserDTO userDTO) {
      logger.info("checkAndSaveUser : " + userDTO);
     UserDTO toReturn =  userService.checkAndSaveUser(userDTO);

    return toReturn;
    }


}
