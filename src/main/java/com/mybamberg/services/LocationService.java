package com.mybamberg.services;

import com.mybamberg.entities.Event;
import com.mybamberg.entities.Location;
import com.mybamberg.repositories.EventRepository;
import com.mybamberg.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by christian on 08.05.16.
 */
@Service
public class LocationService {

    @Autowired
    private LocationRepository locationRepository;

    // actually not used but maybe in the future
    @Transactional(readOnly = true)
    public List<Location> getAllLocations(){
        return locationRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Location getOneLocations(Long l){
        return locationRepository.findOne(l);
    }


}
