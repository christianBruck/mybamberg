package com.mybamberg.services;

import com.mybamberg.dto.EventDTO;
import com.mybamberg.dto.PreferencesDTO;
import com.mybamberg.dto.RouteDTO;
import com.mybamberg.entities.Event;
import com.mybamberg.entities.Route;
import com.mybamberg.entities.User;
import com.mybamberg.repositories.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christian on 09.06.16.
 */
@Service
public class RoutenService {

    @Autowired
    private EventRepository eventsRepository;
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private WeatherRepository weatherRepository;
    @Autowired
    private  UserRepository userRepository;

    @Autowired
    private EventService eventService;
    @Autowired
    private RouteRepository routeRepository;


   // private Logger logger = LoggerFactory.getLogger(RoutenService.class);

    @Transactional
   public  RouteDTO saveFinalRoute(RouteDTO routeDTO){
      User user=  userRepository.findByAndroidId(routeDTO.getAndroidId());
      RouteDTO routeDTO1 =  eventService.saveRouteAndTransformIntoRouteDTO(routeDTO.getEventList(),user);
        return routeDTO1;
    }

    public RouteDTO checkRouteValidityByAndroidID(String androidId){
       // logger.info("enter checkRouteValidityByAndroidID : " +  androidId);
        User user=  userRepository.findByAndroidId(androidId);
        //logger.info("user found : " + user);
        Route route = routeRepository.findByUser(user);
        Route myRoute =  checkEventValidity(route);
        RouteDTO routeDTO = new RouteDTO();
        routeDTO.setEventList(myRoute.getEventList());
        return routeDTO;

    }

    private Route checkEventValidity(Route route) {
       // logger.info("checkEventValidity " +  route);
        List<Event> myEventList = route.getEventList();
        for (Event event : myEventList) {
            if (eventService.checkWeather(event)) {
                event.setValid(true);
            } else {
                event.setValid(false);
            }
        }
        return route;
    }



}
