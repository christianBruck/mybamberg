package com.mybamberg.services;

import com.mybamberg.entities.Event;
import com.mybamberg.entities.Location;
import com.mybamberg.entities.Tag;
import com.mybamberg.repositories.EventRepository;
import com.mybamberg.repositories.LocationRepository;
import com.mybamberg.repositories.TagRepository;
import com.mybamberg.repositories.WeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by christian on 15.05.16.
 */
@Service
public class TagService {
    @Autowired
    private EventRepository eventsRepository;
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private WeatherRepository weatherRepository;

    // actually not used but in the future maybe
    @Transactional(readOnly = true)
    public List<Tag> getAllTags() {return tagRepository.findAll();}

    @Transactional(readOnly = true)
    public Tag getOneTag(Long l) {
        return tagRepository.findOne(l);
    }
}
