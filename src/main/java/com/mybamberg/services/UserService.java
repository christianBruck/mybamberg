package com.mybamberg.services;

import com.mybamberg.dto.UserDTO;
import com.mybamberg.entities.Category;
import com.mybamberg.entities.User;
import com.mybamberg.repositories.CategoryRepository;
import com.mybamberg.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christian on 25.07.16.
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    /**
     * The method first verifies if the user is new, if not then just the usersetings will be updated. If the user is new then the user
     * object will just be stored in the database
     * @param userDTO
     * @return
     */
    @Transactional
    public UserDTO checkAndSaveUser(UserDTO userDTO) {
        User user = userRepository.findByAndroidId(userDTO.getAndroidId());
        if (user != null) { // if the user is known, his new settigs will be saved
            User newUser = updateUserDaten(user, userDTO);
            return transformEntitiesToDTO(userRepository.save(newUser));
        }// otherwise the user will be  just saved
        User toSave = transformDTOToEntities(userDTO);
        return transformEntitiesToDTO(userRepository.save(toSave));
    }

    private UserDTO transformEntitiesToDTO(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setCategoryList(user.getCategoryList());
        userDTO.setStartdate(user.getStartdate());
        userDTO.setEnddate(user.getEnddate());
        userDTO.setAndroidId(user.getAndroidId());
        return userDTO;
    }


    private User transformDTOToEntities(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setAndroidId(userDTO.getAndroidId());
        user.setStartdate(userDTO.getStartdate());
        user.setEnddate(userDTO.getEnddate());
        List<Category> toSave = new ArrayList<>();
        for (Category cat : userDTO.getCategoryList()) {
            toSave.add(categoryRepository.findByCategoryname(cat.getCategoryname()));
        }

        user.setCategoryList(toSave);
        ;

        return user;

    }

    private User updateUserDaten(User user, UserDTO userDTO) {
        user.setStartdate(userDTO.getStartdate());
        user.setEnddate(userDTO.getEnddate());
        List<Category> toSave = new ArrayList<>();
        for (Category cat : userDTO.getCategoryList()) {
            toSave.add(categoryRepository.findByCategoryname(cat.getCategoryname()));
        }

        user.setCategoryList(toSave);
        return user;
    }


}
