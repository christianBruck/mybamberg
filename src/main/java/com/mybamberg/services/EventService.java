package com.mybamberg.services;

import com.mybamberg.dto.RouteDTO;
import com.mybamberg.entities.*;
import com.mybamberg.repositories.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by christian on 01.05.16.
 */
@Service
public class EventService {

   // private static final Logger logger = LoggerFactory.getLogger(EventService.class);

    private static String GUT = "gut";

    private static String SCHLECHT = "schlecht";

    @Autowired
    private EventRepository eventsRepository;
    @Autowired
    private WeatherRepository weatherRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RouteRepository routeRepository;

    /**
     * This method is the core of the project. It select events wich are compatible with the usersettings and the weatherforecast.
     * @param androidId
     * @return
     */
    @Transactional(readOnly = true)
    public RouteDTO getEventsForUser(String androidId) {
        User user = userRepository.findByAndroidId(androidId);
        List<Event> eventsToReturn = new ArrayList<>();
        List<Event> eventsToReturnReally = new ArrayList<>();
        Date startDateUser = user.getStartdate();
        Date endDateUser = user.getEnddate();
        List<Category> categoryList = user.getCategoryList();
        //first step: select events wich match the usersettings
        for (Category category : categoryList) {
            List<Event> eventList = eventsRepository.findMyEvents(category, startDateUser, endDateUser);
            eventsToReturn.addAll(eventList);
        }
        //second step: check if the choosen events are compatible with the weatherforecast
        for (Event event : eventsToReturn) {
            if (checkWeather(event)) {
                eventsToReturnReally.add(event);
            }
        }
        return saveRouteAndTransformIntoRouteDTO(eventsToReturnReally, user);
    }

    /**
     *
     * @param eventList
     * @return
     */
    public RouteDTO saveRouteAndTransformIntoRouteDTO(List<Event> eventList, User user) {

        Route route = routeRepository.findByUser(user);
        if (route != null) {
            route.setEventList(eventList);
            routeRepository.save(route);
        } else {

             Route route1 = new Route();
            route1.setUser(user);
            route1.setEventList(eventList);
            routeRepository.save(route1);
        }
        RouteDTO routeDTO = new RouteDTO();
        routeDTO.setEventList(eventList);

        return routeDTO;
    }

    /**
     *
     * @param event
     * @return
     */
    @Transactional
    public boolean checkWeather(Event event) {
       // logger.info("checkWeather for event : " + event);
        Date startDate = event.getStartdate();
        Date endDate = event.getEnddate();
        List<Weather> weatherList = weatherRepository.findByDateBetween(startDate, endDate);
        weatherList.add(weatherRepository.findOneByDateBefore(startDate));

        for (Weather weatherA:weatherList) {
            if ( endDate.equals(weatherA.getDate())) {
                weatherList.remove(weatherA);
            }
        }

        boolean isCompatible = true;

        // wenn das event ueberhaupt gutes wetter bedarf such ich die wetter-dates dazu
        if (event.getSimpleWeather().getName().equalsIgnoreCase(GUT)) {
            //logger.info("Event need good weather " +  event);
            for (Weather weather : weatherList) {
                if (!weather.getSimpleWeather().getName().equalsIgnoreCase(GUT)) {
                    isCompatible = false;
                    break;
                }
            }
        }
       // logger.info("will return weather compatible " +  isCompatible);
        return isCompatible;
    }


}
