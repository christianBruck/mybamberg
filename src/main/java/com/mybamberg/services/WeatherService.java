package com.mybamberg.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybamberg.entities.Event;
import com.mybamberg.entities.SimpleWeather;
import com.mybamberg.entities.Weather;
import com.mybamberg.repositories.SimpleWeatherRepository;
import com.mybamberg.repositories.WeatherRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;
import java.util.*;

/**
 * Created by christian on 02.07.16.
 */
@Service
public class WeatherService {

   // private static Logger logger = LoggerFactory.getLogger(WeatherService.class);
    private static final String SCHLECHT = "schlecht";
    private static final String GUT = "gut";
    @Autowired
    private WeatherRepository weatherRepository;
    @Autowired
    private EventService eventService;
    @Autowired
    private SimpleWeatherRepository simpleWeatherRepository;
    String url = "http://api.openweathermap.org/data/2.5/forecast?id=3220844&appid=79f5eb88f23cf57aff5334389b0f6b3d";
    Map<Integer, String> simpleWeatherMap = new HashMap<Integer, String>();

    /**
     * calls the Openweather API for the latest forecast for the city Bamberg. Every three-hour forecast section is stored as a weather
     * data in the database
     * @throws IOException
     */
    @Scheduled(fixedRate = 600000)
    @Transactional
    public void requestWeatherData() throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        URI targetUrl = UriComponentsBuilder.fromUriString(url).build().toUri();
        String response = restTemplate.getForObject(targetUrl, String.class);
        //ObjectMapper is ne Jackson Klasse die zum Convertieren von JSON zu Java dient
        ObjectMapper objectMapper = new ObjectMapper();
        //Method to deserialize JSON content as tree expressed using set of JsonNode instances.
        JsonNode rootNode = objectMapper.readTree(response);
        //Base class for all JSON nodes, which form the basis of JSON Tree Model that Jackson implements.
        // One way to think of these nodes is to consider them similar to DOM nodes in XML DOM trees.
        JsonNode listArray = rootNode.path("list");//gibt mir list als ein node zurueck
        Iterator iteratorListArray = listArray.iterator();//Iterator lauft ueber die Liste von einzelnen 3-Stunden JSON's
       // logger.debug("count Weather objects " + listArray.size());
        while (iteratorListArray.hasNext()) {
            JsonNode nextNode = (JsonNode) iteratorListArray.next();
            Long dateAsLong = nextNode.path("dt").longValue();
            Weather weather = getWeatherByDate(new Date(dateAsLong * 1000));
            JsonNode weatherNode = nextNode.path("weather");
            // im weathernode is der array weather gespeichert, der aber nur ein
            // {json} hat , koennte bei der konstruktion durch , getrennt mehrere haben, deswegen get(0)
            weather.setDescription(weatherNode.get(0).get("description").textValue());
            int weatherID = weatherNode.get(0).get("id").intValue();
            weather.setWeatherApiId(weatherNode.get(0).get("id").intValue());
            SimpleWeather simpleWeather = getByWeatherId(weatherID);
            weather.setSimpleWeather(simpleWeather);
           // logger.debug("Thats my weather now : " +  weather);
            weatherRepository.save(weather);
        }
    }

    @Transactional
    private Weather getWeatherByDate(Date date){
        Weather weather = weatherRepository.findByDate(date);
        if(weather == null){
            weather = new Weather();
            weather.setDate(date);
        }
        return weather;
    }

    @Transactional
    private SimpleWeather getByWeatherId(int weatherID) {
       String zustand =  simpleWeatherMap.get(weatherID);
       return simpleWeatherRepository.findByName(zustand);
    }

    @PostConstruct
    public void fillTheHashMap() {

        simpleWeatherMap.put(200, SCHLECHT);
        simpleWeatherMap.put(201, SCHLECHT);
        simpleWeatherMap.put(202, SCHLECHT);
        simpleWeatherMap.put(210, SCHLECHT);
        simpleWeatherMap.put(211, SCHLECHT);
        simpleWeatherMap.put(212, SCHLECHT);
        simpleWeatherMap.put(221, SCHLECHT);
        simpleWeatherMap.put(230, SCHLECHT);
        simpleWeatherMap.put(231, SCHLECHT);
        simpleWeatherMap.put(232, SCHLECHT);
        //Drizzle
        simpleWeatherMap.put(300, SCHLECHT);
        simpleWeatherMap.put(301, SCHLECHT);
        simpleWeatherMap.put(302, SCHLECHT);
        simpleWeatherMap.put(310, SCHLECHT);
        simpleWeatherMap.put(312, SCHLECHT);
        simpleWeatherMap.put(313, SCHLECHT);
        simpleWeatherMap.put(314, SCHLECHT);
        simpleWeatherMap.put(321, SCHLECHT);
        //Rain
        simpleWeatherMap.put(500, SCHLECHT);
        simpleWeatherMap.put(501, SCHLECHT);
        simpleWeatherMap.put(502, SCHLECHT);
        simpleWeatherMap.put(503, SCHLECHT);
        simpleWeatherMap.put(504, SCHLECHT);
        simpleWeatherMap.put(511, SCHLECHT);
        simpleWeatherMap.put(520, SCHLECHT);
        simpleWeatherMap.put(521, SCHLECHT);
        simpleWeatherMap.put(522, SCHLECHT);
        simpleWeatherMap.put(531, SCHLECHT);
        //Snow
        simpleWeatherMap.put(600, SCHLECHT);
        simpleWeatherMap.put(601, SCHLECHT);
        simpleWeatherMap.put(602, SCHLECHT);
        simpleWeatherMap.put(611, SCHLECHT);
        simpleWeatherMap.put(612, SCHLECHT);
        simpleWeatherMap.put(615, SCHLECHT);
        simpleWeatherMap.put(616, SCHLECHT);
        simpleWeatherMap.put(620, SCHLECHT);
        simpleWeatherMap.put(621, SCHLECHT);
        simpleWeatherMap.put(622, SCHLECHT);
        //Atmosphere
        simpleWeatherMap.put(701, SCHLECHT);
        simpleWeatherMap.put(711, SCHLECHT);
        simpleWeatherMap.put(721, SCHLECHT);
        simpleWeatherMap.put(731, SCHLECHT);
        simpleWeatherMap.put(741, SCHLECHT);
        simpleWeatherMap.put(751, SCHLECHT);
        simpleWeatherMap.put(761, SCHLECHT);
        simpleWeatherMap.put(762, SCHLECHT);
        simpleWeatherMap.put(771, SCHLECHT);
        simpleWeatherMap.put(781, SCHLECHT);
        //Clear
        simpleWeatherMap.put(800, GUT);
        //Clouds
        simpleWeatherMap.put(801, GUT);
        simpleWeatherMap.put(802, GUT);
        simpleWeatherMap.put(803, GUT);
        simpleWeatherMap.put(804, GUT);
        //Extreme
        simpleWeatherMap.put(900, SCHLECHT);
        simpleWeatherMap.put(901, SCHLECHT);
        simpleWeatherMap.put(902, SCHLECHT);
        simpleWeatherMap.put(903, SCHLECHT);
        simpleWeatherMap.put(904, SCHLECHT);
        simpleWeatherMap.put(905, SCHLECHT);
        simpleWeatherMap.put(906, SCHLECHT);
        //Aditional
        simpleWeatherMap.put(951, SCHLECHT);
        simpleWeatherMap.put(952, SCHLECHT);
        simpleWeatherMap.put(953, SCHLECHT);
        simpleWeatherMap.put(954, SCHLECHT);
        simpleWeatherMap.put(955, SCHLECHT);
        simpleWeatherMap.put(956, SCHLECHT);
        simpleWeatherMap.put(957, SCHLECHT);
        simpleWeatherMap.put(958, SCHLECHT);
        simpleWeatherMap.put(959, SCHLECHT);
        simpleWeatherMap.put(960, SCHLECHT);
        simpleWeatherMap.put(961, SCHLECHT);
        simpleWeatherMap.put(962, SCHLECHT);
    }

}

