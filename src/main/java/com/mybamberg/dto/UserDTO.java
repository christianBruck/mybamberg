package com.mybamberg.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mybamberg.entities.Category;


import java.util.Date;
import java.util.List;

/**
 * Created by christian on 25.07.16.
 */
public class UserDTO {
    private Long id;

    private String androidId;

    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss Z")
    private Date startdate;
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss Z")
    private Date enddate;


    private List<Category> categoryList;






    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }




}
