package com.mybamberg.dto;

import com.mybamberg.entities.Event;

import java.util.List;

/**
 * Created by christian on 13.06.16.
 */
public class RouteDTO {



    private List<Event> eventList;
    private String androidId;


    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    @Override
    public String toString() {
        return "RouteDTO{" +
                "eventList=" + eventList +
                '}';
    }
}
