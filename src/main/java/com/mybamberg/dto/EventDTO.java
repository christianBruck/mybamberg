package com.mybamberg.dto;

import com.mybamberg.entities.Location;
import com.mybamberg.entities.Tag;
import com.mybamberg.entities.Weather;

import java.io.Serializable;
import java.util.Date;

public class EventDTO implements Serializable {
    private Long id;

    private String eventname;

    private Long locationId;

    private Tag tag;

    private Weather weather;

    private Date date;

    private boolean valid;

    private String pictureURL;


    public EventDTO() {
    }

    public boolean isValid() {
        return valid;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


}
