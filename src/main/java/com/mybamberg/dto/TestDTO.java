package com.mybamberg.dto;

/**
 * Created by christian on 08.06.16.
 */
public class TestDTO {


    public String testString;

    public String getTestString() {
        return testString;
    }

    public void setTestString(String testString) {
        this.testString = testString;
    }


    @Override
    public String toString() {
        return "TestDTO{" +
                "testString='" + testString + '\'' +
                '}';
    }
}
