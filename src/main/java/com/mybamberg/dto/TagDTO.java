package com.mybamberg.dto;

import java.io.Serializable;

/**
 * Created by christian on 16.05.16.
 */
public class TagDTO implements Serializable {


    private Long ID;
    private String tagName;


    public TagDTO(){

    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
