insert into simple_weather (name) VALUE("gut");
INSERT INTO simple_weather (name) VALUE ("schlecht");

insert into category (categoryname)VALUE ("sport");
insert into category (categoryname)VALUE ("musikfest");
insert into category (categoryname)VALUE ("straßenfest");
insert into category (categoryname)VALUE ("kunst");
insert into category (categoryname)VALUE ("museum");
insert into category (categoryname)VALUE ("univeranstaltung");
insert into category (categoryname)VALUE ("informationsveranstaltung");
insert into category (categoryname)VALUE ("nachtleben");
insert into category (categoryname)VALUE ("kirche_kloster");
insert into category (categoryname)VALUE ("theater");
/*
insert into tag(tag_name)VALUE ("einmalig");
insert into tag(tag_name)VALUE ("gratis");
insert into tag(tag_name)VALUE ("für Kinder geeignet");
insert into tag(tag_name)VALUE ("regelmäßig");
insert into tag(tag_name)VALUE ("wöchentlich");
insert into tag(tag_name)VALUE ("einmal im Jahr");
insert into tag(tag_name)VALUE ("letzter Tag");*/



insert into bamberg.location (locationname,locationaddress)VALUE ("Ehemalige Dominikanerkirche", "Bamberg Dominikanerstraße 2");
insert into bamberg.location (locationname,locationaddress)VALUE ("Altes Rathaus", "Bamberg Altes Rathaus");
insert into bamberg.location (locationname,locationaddress)VALUE (" Gasthaus Schlenkerle", "Bamberg Dominikanerstraße 6");
insert into bamberg.location (locationname,locationaddress)VALUE ("Universität", "Bamberg Kapuzinerstraße 16");

-- insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-09-11 11:00:00","2016-09-11 13:00:00","9","Tag des offenen Denkmals","2","1","http://192.168.2.102:8080/denkmaltag.jpg",b'1',"Langhaus von 1401, Chor von 1416. 1803 zusammen mit dem Kloster säkularisiert, danach Kaserne. Kirche seit 2002 Aula der Universität Bamberg.");

insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 12:00:00","2016-09-19 21:00:00","5","Kinderspass am Gabelmoo !","2","2","bamberg_2.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 12:00:00","2016-09-19 20:00:00","4","Bamberg zaubert again","2","3","bamberg_3.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 02:00:00","2016-09-19 20:00:00","5","Jazzfest Bamberg rockt!","2","3","bamberg_4.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 02:00:00","2016-09-19 20:00:00","5","Feki Obstaktion !","2","3","bamberg_5.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 17:00:00","2016-09-19 20:00:00","7","Englisches Teater !","2","3","bamberg_5.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 18:00:00","2016-09-19 20:00:00","8","Diözesen Museum Ausstellung","2","4","bamberg_6.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 02:00:00","2016-09-19 23:00:00","7","Kinderfest haha am Gabelmoo","2","2","bamberg_7.jpg",b'1');

insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 09:00:00","2016-09-19 20:00:00","1","Gondeln am Kranen","1","2","bamberg_8.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 19:00:00","2016-09-19 21:00:00","2","Tischkickerturnier WM","2","4","bamberg_9.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 19:00:00","2016-09-19 20:00:00","3","Frisbee im Volkspark","2","3","bamberg_10.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 12:00:00","2016-09-19 23:00:00","5","Technoparty 4/4 Takt","2","1","bamberg_11.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 16:00:00","2016-09-19 22:00:00","5","Unifest in der Mensa Au","2","4","bamberg_9.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 13:00:00","2016-09-19 20:00:00","7","SPD-Jusos informiert!","2","1","bamberg_8.jpg",b'1');
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid)VALUE ("2016-09-19 18:00:00","2016-09-19 20:00:00","9","Wunderburg Kerwa fun","2","3","bamberg_7.jpg",b'1');


/*
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("1","2");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("1","3");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("1","6");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("1","7");*/
