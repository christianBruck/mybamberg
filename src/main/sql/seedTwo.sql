insert into simple_weather (name) VALUE("gut");
INSERT INTO simple_weather (name) VALUE ("schlecht");

insert into category (categoryname)VALUE ("sport");
insert into category (categoryname)VALUE ("musikfest");
insert into category (categoryname)VALUE ("straßenfest");
insert into category (categoryname)VALUE ("kunst");
insert into category (categoryname)VALUE ("museum");
insert into category (categoryname)VALUE ("univeranstaltung");
insert into category (categoryname)VALUE ("informationsveranstaltung");
insert into category (categoryname)VALUE ("nachtleben");
insert into category (categoryname)VALUE ("kirche_kloster");
insert into category (categoryname)VALUE ("theater");

insert into tag(tag_name)VALUE ("einmalig");
insert into tag(tag_name)VALUE ("gratis");
insert into tag(tag_name)VALUE ("für Kinder geeignet");
insert into tag(tag_name)VALUE ("regelmäßig");
insert into tag(tag_name)VALUE ("wöchentlich");
insert into tag(tag_name)VALUE ("einmal im Jahr");
insert into tag(tag_name)VALUE ("letzter Tag");



insert into bamberg.location (locationname,locationaddress)VALUE ("Innenhof an der Breitenau 9", "Bamberg Markusplatz");
insert into bamberg.location (locationname,locationaddress)VALUE ("Tourist Information", "Bamberg Geyerswörthstraße 5");
insert into bamberg.location (locationname,locationaddress)VALUE ("Gasthaus Schlenkerle", "Bamberg Dominikanerstraße 6");
insert into bamberg.location (locationname,locationaddress)VALUE ("Universität", "Bamberg Kapuzinerstraße 16");

insert into bamberg.location (locationname,locationaddress)VALUE ("Profil Lounge", "Bamberg Lange Str. 13");
insert into bamberg.location (locationname,locationaddress)VALUE ("Tourist Treffpunkt", "Bamberg Geyerswörthstraße 7");
insert into bamberg.location (locationname,locationaddress)VALUE ("Sound-n-Arts Club", "Bamberg Obere Sandstraße 20");
insert into bamberg.location (locationname,locationaddress)VALUE ("Karmelitenkirche", "Bamberg Karmelitenplatz 1");

insert into bamberg.location (locationname,locationaddress)VALUE ("Bootshaus im Hain", "Bamberg Mühlwörth 18");
insert into bamberg.location (locationname,locationaddress)VALUE ("Club Kaulberg", "Bamberg Unterer Kaulberg 36");
insert into bamberg.location (locationname,locationaddress)VALUE ("Hotel Bamberger Hof", "Bamberg Schönleinsplatz 4");
insert into bamberg.location (locationname,locationaddress)VALUE ("Theater am Michelsberg", "Bamberg Michelsberg 10");

-- insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-09-11 11:00:00","2016-09-11 13:00:00","9","Tag des offenen Denkmals","2","1","http://192.168.2.102:8080/denkmaltag.jpg",b'1',"Langhaus von 1401, Chor von 1416. 1803 zusammen mit dem Kloster säkularisiert, danach Kaserne. Kirche seit 2002 Aula der Universität Bamberg.");

insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 15:00:00","2016-10-08 18:00:00","1","Reise um die Welt","2","1","bamberg_2.jpg",b'1',"Wir laden die Kinder ein auf eine „Reise um die Welt“! Wir erklimmen die Spitze des Mount Everest, feiern das Farbenfest „Holi“, spielen Baseball, machen Schweizer Raclette, tragen Kopftücher, verkleiden uns als Pharaonen und basteln Sombreros.");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 10:00:00","2016-10-08 12:00:00","1","Erlebnisführung: Faszination Weltkulturerbe","1","2","bamberg_3.jpg",b'1',"Erleben Sie die Magie des Weltkulturerbes und nehmen Sie an unseren täglichen 2 stündigen Führungen teil");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid, description)VALUE ("2016-10-08 02:00:00","2016-10-08 20:00:00","2","Jazzfest Bamberg rockt!","1","3","bamberg_4.jpg",b'1',"Das jährliche Jazzfest in der Innenenstadt von bamberg mit Gästen aus der ganzen Welt!");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 02:00:00","2016-10-08 20:00:00","3","Feki Obstaktion !","1","4","bamberg_5.jpg",b'1',"Damit die Studenten zwischen den ganzen Bieren und Vorlesungen auch mal was Gesundes im Magen haben");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 17:00:00","2016-10-08 20:00:00","4","Englisches Teater !","2","5","bamberg_5.jpg",b'1',"Die Studentengruppe die es so richtig auf English krachen lässt.");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 18:00:00","2016-10-08 20:00:00","5","Innenstadt Museum Reptilien-Ausstellung","2","6","bamberg_6.jpg",b'1',"Reptilien aus der ganzen Welt! Diese Tiere faszinieren durch ihre urtümliche Erscheinung! ");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 02:00:00","2016-10-08 23:00:00","6","Disney Junior Mitmachkino","2","7","bamberg_7.jpg",b'1',"An alle kleinen Mickymäuse: Bei diesem Kino-Event müssen die jüngsten Kinofans nicht mucksmäuschenstill im Kinosessel sitzen.");

insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 09:00:00","2016-10-08 20:00:00","7","Gondeln am Kranen","1","8","bamberg_8.jpg",b'1',"Ist es Venedig? Nein es ist Bamberg, Kleinvenedig in Fraken!");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 19:00:00","2016-10-08 21:00:00","8","Tischkickerturnier WM","2","9","bamberg_9.jpg",b'1',"Der Informatikersport auch für Nichtinformatiker! Alle sind Willkommen!");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 19:00:00","2016-10-08 20:00:00","9","Frisbee im Volkspark","1","10","bamberg_10.jpg",b'1',"Die magische Scheibe dreht durch!");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("22016-10-08 12:00:00","2016-10-08 23:00:00","5","Salsa 24/7","2","11","bamberg_11.jpg",b'1',"Salsamadness in Bamberg");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 16:00:00","2016-10-08 22:00:00","10","Improtheater am Michaelsberg","2","12","bamberg_9.jpg",b'1',"Mit den allseits bekannte Stars der Bamberger Improszene");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 13:00:00","2016-10-08 20:00:00","8","SPD-Jusos informiert!","2","10","bamberg_8.jpg",b'1',"TTIP ja nein vielleicht---- wir werden es erfahren :-)");
insert into bamberg.event (startdate,enddate,category_id,eventname,simple_weather_id,location_id,pictureURL,valid,description)VALUE ("2016-10-08 18:00:00","2016-10-08 20:00:00","3","Wunderburg Kerwa fun","1","5","bamberg_7.jpg",b'1',"Tracht, Musik, Karusell, Bier, Tracht ,Musik, Karusell, Bier,.....");




insert into bamberg.event_tag(event_id, taglist_id) VALUE ("1","2");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("1","3");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("1","6");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("1","7");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("2","2");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("2","3");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("2","4");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("3","1");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("3","2");


insert into bamberg.event_tag(event_id, taglist_id) VALUE ("4","2");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("4","4");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("5","2");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("5","3");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("5","1");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("5","4");


insert into bamberg.event_tag(event_id, taglist_id) VALUE ("6","1");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("6","3");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("6","5");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("7","7");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("7","2");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("8","1");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("8","2");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("8","3");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("8","2");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("8","5");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("8","6");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("9","7");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("9","1");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("9","4");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("10","6");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("10","5");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("10","2");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("10","3");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("11","6");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("12","7");

insert into bamberg.event_tag(event_id, taglist_id) VALUE ("12","1");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("13","4");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("13","6");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("13","7");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("13","2");
insert into bamberg.event_tag(event_id, taglist_id) VALUE ("13","3");



