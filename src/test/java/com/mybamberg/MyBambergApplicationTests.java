package com.mybamberg;

import com.mybamberg.entities.*;
import com.mybamberg.repositories.*;
import com.mybamberg.services.EventService;
import com.mybamberg.services.RoutenService;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static junit.framework.TestCase.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyBambergApplication.class)
@TestPropertySource(locations = "classpath:test.properties")
public class MyBambergApplicationTests {


    private static List<Category> allCategories = new ArrayList<>();
    private static List<SimpleWeather> allSimpleWeathers = new ArrayList<>();
    private static List<Weather> allWeather = new ArrayList<>();

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private EventService eventService;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private SimpleWeatherRepository simpleWeatherRepository;
    @Autowired
    private WeatherRepository weatherRepository;


    /**
     * 2 events haben den gleichen start date
     *
     */

    public void testEventsWithSameStartDate(){

    }


    /**
     * ein event geht über sehr lange zeit zb 8 stunden, und der user hat nur 2 stunden zeit,
     * erwartung: event ist nicht in der Liste
     */
    public void testLongRunEventShortTimeUser(){

    }

    /**
     * es gibt eine menge von events die sehr lange gehen und nicht in das Zeitfenster des users passen,
     * nur ein event passt in das zeitfenster des users
     * erwartung : nur ein event in der liste
     */
    public void manyLongRunEventsOneFitsUsersTime(){

    }



    @Test
    public void testOneCategoryNodateLimit() {
       setup();
        User user = new User();
        user = createUserDateTime();
        user.setStartdate(createFuturDate(0));
        user.setEnddate(createFuturDate(24));
        user.setCategoryList(findUserCategoriesMuseumAndNatur());
        //List<Event> eventList = eventService.getEventsForUser(user);

        //assertEquals(1, eventList.size());

        tearDown();
    }

    @Test
    public void testNotNull() {
        setup();
        User user = new User();
        user = createUserDateTime();
        user.setStartdate(createFuturDate(0));
        user.setEnddate(createFuturDate(10));
        user.setCategoryList(findUserCategoriesMuseumAndNatur());
        //List<Event> eventList = eventService.getEventsForUser(user);
        //assertNotNull(eventList);
        tearDown();
    }



    @Test
    public void testNoCategory() {
        setup();
        User user = new User();
        user = createUserDateTime();
        user.setCategoryList(generateEmptyList());
        user.setStartdate(createFuturDate(0));
        user.setEnddate(createFuturDate(10));
        //List<Event> eventList = eventService.getEventsForUser(user);
        //assertNotNull(eventList);
        tearDown();

    }

    @Test
    public void testByDateAndCategory() {
        setup();
        User user = new User();
        user = createUserDateTime();
        user.setCategoryList(findAllUserCategories());
        // List<Event> eventList = eventService.getEventsForUser(user);
        //assertEquals(3, eventList.size());
        tearDown();
    }






    @Transactional
    public void setup() {


            generateSimpleWeather();
            generateWeather();

            List<Tag> tagList = generateAllTags();
            List<Category> categoryList = generateAllcategories();
            allCategories = categoryList;

            for (int i = 0; i < 5; i++) {
                Event event = new Event();
                event.setCategory(categoryList.get(i));
                event.setSimpleWeather(allSimpleWeathers.get(i % 2));
                String eventname = "Event Nr: " + i;
                event.setEventname(eventname);
                event.setStartdate(createFuturDate(i));
                event.setEnddate(createFuturDate(i + 2));
                eventRepository.save(event);

            }


    }

    @Transactional
    public void tearDown(){
        eventRepository.deleteAll();
        categoryRepository.deleteAll();
        weatherRepository.deleteAll();
        simpleWeatherRepository.deleteAll();
        tagRepository.deleteAll();

    }



    /**
     * creates a date in future from now
     *
     * @param hour in future
     * @return date
     */
    private Date createFuturDate(int hour) {
        LocalDateTime localDate = LocalDateTime.now();
        LocalDateTime inFuture = localDate.plus(hour, ChronoUnit.HOURS);
        LocalDateTime adjustedInFuture = LocalDateTime.of(inFuture.getYear(),inFuture.getMonth(),inFuture.getDayOfMonth(),inFuture.getHour(),inFuture.getMinute());
        Instant instant = adjustedInFuture.atZone(ZoneId.systemDefault()).toInstant();
        Date date = Date.from(instant);
        return date;
    }

    private User createUserDateTime() {
        User user = new User();
        user.setStartdate(createFuturDate(0));
        user.setEnddate(createFuturDate(40));
        return user;
    }


    private Tag generateTag(String tagname) {
        Tag tag = new Tag();
        tag.setTagName(tagname);
        return tagRepository.save(tag);


    }

    private Category generatCategory(String categoryname) {
        Category category = new Category();
        category.setCategoryname(categoryname);
        return categoryRepository.save(category);


    }

    private List<SimpleWeather> generateSimpleWeather() {

        SimpleWeather gut = new SimpleWeather();
        gut.setName("gut");
        simpleWeatherRepository.save(gut);
        SimpleWeather schlecht = new SimpleWeather();
        schlecht.setName("schlecht");
        simpleWeatherRepository.save(schlecht);
        List<SimpleWeather> toReturn = simpleWeatherRepository.findAll();
        allSimpleWeathers = toReturn;
        return toReturn;

    }


    private List<Tag> generateAllTags() {
        List<Tag> tagList = new ArrayList<>();
        tagList.add(generateTag("gratis"));
        tagList.add(generateTag("einmalig"));
        tagList.add(generateTag("familienfreundlich"));
        tagList.add(generateTag("univeranstaltung"));
        tagList.add(generateTag("regelmaessig"));
        return tagList;
    }

    private List<Category> findAllUserCategories() {
        return allCategories;
    }

    private List<Category> findUserCategoriesMuseumAndNatur() {
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(categoryRepository.findByCategoryname("Museum"));
        categoryList.add(categoryRepository.findByCategoryname("Natur"));

        return categoryList;
    }


    private List<Category> generateAllcategories() {
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(generatCategory("Straßenfest"));
        categoryList.add(generatCategory("Musikfest"));
        categoryList.add(generatCategory("Museum"));
        categoryList.add(generatCategory("Natur"));
        categoryList.add(generatCategory("Sehenswürdigkeiten"));


        return categoryList;
    }

    private List<Category> generateEmptyList() {
        List<Category> emptyList = new ArrayList<>();
        return emptyList;
    }

    private List<Weather> generateWeather() {
        for (int i = 0; i < 40; i++) {
            Weather weather = new Weather();
            weather.setDate(createFuturDate(i * 3));
            weather.setSimpleWeather(allSimpleWeathers.get(i % 2));
            weatherRepository.save(weather);
        }
        List<Weather> toReturn = weatherRepository.findAll();
        allWeather = toReturn;
        return toReturn;
    }


}
